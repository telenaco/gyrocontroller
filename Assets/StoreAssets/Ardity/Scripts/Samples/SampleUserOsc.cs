/**
 * Ardity (Serial Communication for Arduino + Unity)
 * Author: Daniel Wilches <dwilches@gmail.com>
 *
 * This work is released under the Creative Commons Attributions license.
 * https://creativecommons.org/licenses/by/2.0/
 */

using UnityEngine;
using System.Collections;

/**
 * Sample for reading using polling by yourself, and writing too.
 */
public class SampleUserOsc : MonoBehaviour
{
    public SerialControllerOsc serialController;

    // Initialization
    void Start()
    {
        serialController = GameObject.Find("SerialControllerOsc").GetComponent<SerialControllerOsc>();

    }

    // Executed each frame
    void Update()
    {

    }
}
