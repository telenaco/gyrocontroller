/**
 * Ardity (Serial Communication for Arduino + Unity)
 * Author: Daniel Wilches <dwilches@gmail.com>
 *
 * This work is released under the Creative Commons Attributions license.
 * https://creativecommons.org/licenses/by/2.0/
 */

using UnityEngine;

using System;
using System.Threading;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.IO.Ports;
using System.IO;

/**
 * This class contains methods that must be run from inside a thread and others
 * that must be invoked from Unity. Both types of methods are clearly marked in
 * the code, although you, the final user of this library, don't need to even
 * open this file unless you are introducing incompatibilities for upcoming
 * versions.
 * 
 * For method comments, refer to the base class.
 */


public class SerialThreadOsc : AbstractSerialThread
{
    byte eot = 0xC0;
    byte slipesc = 0xDB;
    byte slipescend = 0xDC;
    byte slipescesc = 0xDD;

    enum erstate
    {
        CHAR,
        FIRSTEOT,
        SECONDEOT,
        SLIPESC
    }

    //Buffer where a single message must fit
    private byte[] buffer = new byte[1024];
    private Hashtable AddressTable = new Hashtable();
    private ArrayList messagesReceived = new ArrayList();
    private OscMessageHandler AllMessageHandler;

    public SerialThreadOsc(string portName,
                                       int baudRate,
                                       int delayBeforeReconnecting)
        : base(portName, baudRate, delayBeforeReconnecting)
    {
    }

    // < summary >
    // Set the method to call back on when a message with the specified
    // address is received.The method needs to have the OscMessageHandler signature - i.e.
    // void amh(OscMessage oscM)
    // </ summary >
    // < param name = "key" > Address string to be matched</param>   
    // <param name = "ah" > The method to call back on.</param>   
    public void SetAddressHandler(string key, OscMessageHandler ah)
    {

        ArrayList al = (ArrayList)Hashtable.Synchronized(AddressTable)[key];
        if (al == null)
        {
            al = new ArrayList();
            al.Add(ah);
            Hashtable.Synchronized(AddressTable).Add(key, al);
        }
        else
        {
            al.Add(ah);
        }
    }

    /// <summary>
    /// Send an individual OSC message.  Internally takes the OscMessage object and 
    /// serializes it into a byte[] suitable for sending to the PacketIO.
    /// </summary>
    /// <param name="oscMessage">The OSC Message to send.</param>   
    protected override void SendToWire(object _oscM, SerialPort serialPort)
    {
        OscMessage oscM = (OscMessage)_oscM;
        byte[] packet = new byte[1024];
        int length = OscMessageToPacket(oscM, packet, 0, 1024);
        //int length = OscMessagesToPacket(oscM.values, packet, oscM.values.Count);


        // StringBuilder sb = new StringBuilder("after byte[] { ");
        // foreach (var b in packet)
        // {
        //     sb.Append(b + ", ");
        // }
        // sb.Append("}" + packet.Length);
        // Debug.Log(sb.ToString());  

        serialPort.Write(packet, 0, length);
    }


    /// <summary>
    /// Read Thread.  Loops waiting for packets.  When a packet is received, it is 
    /// dispatched to any waiting All Message Handler.  Also, the address is looked up and
    /// any matching handler is called.
    /// </summary>
    protected override object ReadFromWire(SerialPort serialPort)
    {
        int length = ReceivePacket(serialPort);

        if (length > 0)
        {
            ArrayList newMessages = PacketToOscMessages(buffer, length);
            messagesReceived.AddRange(newMessages);
        }
        if (messagesReceived.Count > 0)
        {
            foreach (OscMessage oscM in messagesReceived)
            {

                if (AllMessageHandler != null)
                    AllMessageHandler(oscM);

                ArrayList arrL = (ArrayList)Hashtable.Synchronized(AddressTable)[oscM.address];
                if (arrL != null)
                {
                    foreach (OscMessageHandler h in arrL)
                    {
                        h(oscM);
                    }
                }
            }
            messagesReceived.Clear();
        }

        // returns nothing to the queue 
        byte[] returnBuffer = new byte[100];
        return returnBuffer;
    }

    public int ReceivePacket(SerialPort serialPort)
    {
        int bytes = serialPort.BytesToRead;
        byte[] inPacket = new byte[bytes];
        byte[] outPacket = new byte[bytes];
        // fill the internal buffer
        serialPort.Read(inPacket, 0, bytes);

        // Search for the separator in the buffer

        int outIndex = 0;
        int inIndex = 1;
        int rstate = 0;

        // cycle through the buffer to find the end of the package
        while (inIndex < bytes)
        {
            byte c = inPacket[inIndex];

            if (rstate == (int)erstate.CHAR)
            {
                if (c == slipesc)
                {
                    rstate = (int)erstate.SLIPESC;
                    inIndex++;
                    continue;
                }
                else if (c == eot)
                {
                    //Debug.LogWarning("eot character on middle of received OSC message");
                    break;
                }
                outPacket[outIndex++] = c;
            }
            else if (rstate == (int)erstate.SLIPESC)
            {
                rstate = (int)erstate.CHAR;
                if (c == slipescend)
                    outPacket[outIndex++] = eot;
                else if (c == slipescesc)
                    outPacket[outIndex++] = slipesc;
                else
                ///    Console.Write("test");
                Debug.LogWarning("unknown character on received OSC message");
            }
            else
                Debug.LogWarning("unknown character on received OSC message");

                inIndex++;
        }

        int index = System.Math.Min(inIndex, outPacket.Length);
        System.Array.Copy(outPacket, buffer, index);
        return index;
    }



    /// <summary>
    /// Set the method to call back on when any message is received.
    /// The method needs to have the OscMessageHandler signature - i.e. void amh( OscMessage oscM )
    /// </summary>
    /// <param name="amh">The method to call back on.</param>   
    public void SetAllMessageHandler(OscMessageHandler amh)
    {
        AllMessageHandler = amh;
    }

    /// </summary>
    /// Creates an OscMessage from a string - extracts the address and determines each of the values. 
    /// </summary>
    /// <param name="message">The string to be turned into an OscMessage</param>
    /// <returns>The OscMessage.</returns>
    public  OscMessage StringToOscMessage(string message)
    {
        OscMessage oscM = new OscMessage();
        //Debug.Log("Splitting " + message);
        string[] ss = message.Split(new char[] { ' ' });
        IEnumerator sE = ss.GetEnumerator();
        if (sE.MoveNext())
            oscM.address = (string)sE.Current;
        while (sE.MoveNext())
        {
            string s = (string)sE.Current;
            //Debug.Log("  <" + s + ">");
            if (s.StartsWith("\""))
            {
                StringBuilder quoted = new StringBuilder();
                bool looped = false;
                if (s.Length > 1)
                    quoted.Append(s.Substring(1));
                else
                    looped = true;
                while (sE.MoveNext())
                {
                    string a = (string)sE.Current;
                    //Debug.Log("    q:<" + a + ">");
                    if (looped)
                        quoted.Append(" ");
                    if (a.EndsWith("\""))
                    {
                        quoted.Append(a.Substring(0, a.Length - 1));
                        break;
                    }
                    else
                    {
                        if (a.Length == 0)
                            quoted.Append(" ");
                        else
                            quoted.Append(a);
                    }
                    looped = true;
                }
                oscM.values.Add(quoted.ToString());
            }
            else
            {
                if (s.Length > 0)
                {
                    try
                    {
                        int i = int.Parse(s);
                        //Debug.Log("  i:" + i);
                        oscM.values.Add(i);
                    }
                    catch
                    {
                        try
                        {
                            float f = float.Parse(s);
                            //Debug.Log("  f:" + f);
                            oscM.values.Add(f);
                        }
                        catch
                        {
                            //Debug.Log("  s:" + s);
                            oscM.values.Add(s);
                        }
                    }

                }
            }
        }
        return oscM;
    }

    /// <summary>
    /// Takes a packet (byte[]) and turns it into a list of OscMessages.
    /// </summary>
    /// <param name="packet">The packet to be parsed.</param>
    /// <param name="length">The length of the packet.</param>
    /// <returns>An ArrayList of OscMessages.</returns>
    public  ArrayList PacketToOscMessages(byte[] packet, int length)
    {
        ArrayList messages = new ArrayList();
        ExtractMessages(messages, packet, 0, length);
        return messages;
    }

    /// <summary>
    /// Puts an array of OscMessages into a packet (byte[]).
    /// </summary>
    /// <param name="messages">An ArrayList of OscMessages.</param>
    /// <param name="packet">An array of bytes to be populated with the OscMessages.</param>
    /// <param name="length">The size of the array of bytes.</param>
    /// <returns>The length of the packet</returns>
    public  int OscMessagesToPacket(ArrayList messages, byte[] packet, int length)
    {
        int index = 0;
        if (messages.Count == 1)
            index = OscMessageToPacket((OscMessage)messages[0], packet, 0, length);
        else
        {
            // Write the first bundle bit
            index = InsertString("#bundle", packet, index, length);
            // Write a null timestamp (another 8bytes)
            int c = 8;
            while ((c--) > 0)
                packet[index++]++;
            // Now, put each message preceded by it's length
            foreach (OscMessage oscM in messages)
            {
                int lengthIndex = index;
                index += 4;
                int packetStart = index;
                index = OscMessageToPacket(oscM, packet, index, length);
                int packetSize = index - packetStart;
                packet[lengthIndex++] = (byte)((packetSize >> 24) & 0xFF);
                packet[lengthIndex++] = (byte)((packetSize >> 16) & 0xFF);
                packet[lengthIndex++] = (byte)((packetSize >> 8) & 0xFF);
                packet[lengthIndex++] = (byte)((packetSize) & 0xFF);
            }
        }
        return index;
    }

    /// <summary>
    /// Creates a packet (an array of bytes) from a single OscMessage.
    /// </summary>
    /// <remarks>A convenience method, not requiring a start index.</remarks>
    /// <param name="oscM">The OscMessage to be returned as a packet.</param>
    /// <param name="packet">The packet to be populated with the OscMessage.</param>
    /// <param name="length">The usable size of the array of bytes.</param>
    /// <returns>The length of the packet</returns>
    public  int OscMessageToPacket(OscMessage oscM, byte[] packet, int length)
    {
        return OscMessageToPacket(oscM, packet, 0, length);
    }

    /// <summary>
    /// Creates an array of bytes from a single OscMessage.  Used internally.
    /// </summary>
    /// <remarks>Can specify where in the array of bytes the OscMessage should be put.</remarks>
    /// <param name="oscM">The OscMessage to be turned into an array of bytes.</param>
    /// <param name="packet">The array of bytes to be populated with the OscMessage.</param>
    /// <param name="start">The start index in the packet where the OscMessage should be put.</param>
    /// <param name="length">The length of the array of bytes.</param>
    /// <returns>The index into the packet after the last OscMessage.</returns>
    private  int OscMessageToPacket(OscMessage oscM, byte[] packet, int start, int length)
    {
        int index = start;
        index = InsertString(oscM.address, packet, index, length);
        //if (oscM.values.Count > 0)
        {
            StringBuilder tag = new StringBuilder();
            tag.Append(",");
            int tagIndex = index;
            index += PadSize(2 + oscM.values.Count);

            foreach (object o in oscM.values)
            {
                if (o is int)
                {
                    int i = (int)o;
                    tag.Append("i");
                    packet[index++] = (byte)((i >> 24) & 0xFF);
                    packet[index++] = (byte)((i >> 16) & 0xFF);
                    packet[index++] = (byte)((i >> 8) & 0xFF);
                    packet[index++] = (byte)((i) & 0xFF);
                }
                else
                {
                    if (o is float)
                    {
                        float f = (float)o;
                        tag.Append("f");
                        byte[] buffer = new byte[4];
                        MemoryStream ms = new MemoryStream(buffer);
                        BinaryWriter bw = new BinaryWriter(ms);
                        bw.Write(f);
                        packet[index++] = buffer[3];
                        packet[index++] = buffer[2];
                        packet[index++] = buffer[1];
                        packet[index++] = buffer[0];
                    }
                    else
                    {
                        if (o is string)
                        {
                            tag.Append("s");
                            index = InsertString(o.ToString(), packet, index, length);
                        }
                        else
                        {
                            tag.Append("?");
                        }
                    }
                }
            }
            InsertString(tag.ToString(), packet, tagIndex, length);
        } 

        index = EncodeSLIP(packet, index);

        // sb = new StringBuilder("after byte[] { ");
        // foreach (var b in packet)
        // {
        //     sb.Append(b + ", ");
        // }
        // sb.Append("}" + packet.Length);
        // Debug.Log(sb.ToString());  

        return index;
    }

    /// <summary>
    /// Receive a raw packet of bytes and extract OscMessages from it.  Used internally.
    /// </summary>
    /// <remarks>The packet may contain a OSC message or a bundle of messages.</remarks>
    /// <param name="messages">An ArrayList to be populated with the OscMessages.</param>
    /// <param name="packet">The packet of bytes to be parsed.</param>
    /// <param name="start">The index of where to start looking in the packet.</param>
    /// <param name="length">The length of the packet.</param>
    /// <returns>The index after the last OscMessage read.</returns>
    private int ExtractMessages(ArrayList messages, byte[] packet, int start, int length)
    {
        int index = start;
        switch ((char)packet[start])
        {
            case '/':
                index = ExtractMessage(messages, packet, index, length);
                break;
            case '#':
                string bundleString = ExtractString(packet, start, length);
                if (bundleString == "#bundle")
                {
                    // skip the "bundle" and the timestamp
                    index += 16;
                    while (index < length)
                    {
                        int messageSize = (packet[index++] << 24) + (packet[index++] << 16) + (packet[index++] << 8) + packet[index++];
                        /*int newIndex = */
                        ExtractMessages(messages, packet, index, length);
                        index += messageSize;
                    }
                }
                break;
        }
        return index;
    }

    /// <summary>
    /// Extracts a messages from a packet.
    /// </summary>
    /// <param name="messages">An ArrayList to be populated with the OscMessage.</param>
    /// <param name="packet">The packet of bytes to be parsed.</param>
    /// <param name="start">The index of where to start looking in the packet.</param>
    /// <param name="length">The length of the packet.</param>
    /// <returns>The index after the OscMessage is read.</returns>
    private int ExtractMessage(ArrayList messages, byte[] packet, int start, int length)
    {
        OscMessage oscM = new OscMessage();
        oscM.address = ExtractString(packet, start, length);
        int index = start + PadSize(oscM.address.Length + 1);
        string typeTag = ExtractString(packet, index, length);
        index += PadSize(typeTag.Length + 1);
        //oscM.values.Add(typeTag);
        foreach (char c in typeTag)
        {
            switch (c)
            {
                case ',':
                    break;
                case 's':
                    {
                        string s = ExtractString(packet, index, length);
                        index += PadSize(s.Length + 1);
                        oscM.values.Add(s);
                        break;
                    }
                case 'i':
                    {
                        int i = (packet[index++] << 24) + (packet[index++] << 16) + (packet[index++] << 8) + packet[index++];
                        oscM.values.Add(i);
                        break;
                    }
                case 'f':
                    {
                        byte[] buffer = new byte[4];
                        buffer[3] = packet[index++];
                        buffer[2] = packet[index++];
                        buffer[1] = packet[index++];
                        buffer[0] = packet[index++];
                        MemoryStream ms = new MemoryStream(buffer);
                        BinaryReader br = new BinaryReader(ms);
                        float f = br.ReadSingle();
                        oscM.values.Add(f);
                        break;
                    }
            }
        }
        // add this to queue not to array 
        messages.Add(oscM);
        return index;
    }

    private string Dump(byte[] packet, int start, int length)
    {
        StringBuilder sb = new StringBuilder();
        int index = start;
        while (index < length)
            sb.Append(packet[index++] + "|");
        return sb.ToString();
    }

    /// <summary>
    /// Removes a string from a packet.  Used internally.
    /// </summary>
    /// <param name="packet">The packet of bytes to be parsed.</param>
    /// <param name="start">The index of where to start looking in the packet.</param>
    /// <param name="length">The length of the packet.</param>
    /// <returns>The string</returns>
    private string ExtractString(byte[] packet, int start, int length)
    {
        StringBuilder sb = new StringBuilder();
        int index = start;
        while (packet[index] != 0 && index < length)
            sb.Append((char)packet[index++]);
        return sb.ToString();
    }

    /// <summary>
    /// Inserts a string, correctly padded into a packet.  Used internally.
    /// </summary>
    /// <param name="string">The string to be inserted</param>
    /// <param name="packet">The packet of bytes to be parsed.</param>
    /// <param name="start">The index of where to start looking in the packet.</param>
    /// <param name="length">The length of the packet.</param>
    /// <returns>An index to the next byte in the packet after the padded string.</returns>
    private int InsertString(string s, byte[] packet, int start, int length)
    {
        int index = start;
        foreach (char c in s)
        {
            packet[index++] = (byte)c;
            if (index == length)
                return index;
        }
        packet[index++] = 0;
        int pad = (s.Length + 1) % 4;
        if (pad != 0)
        {
            pad = 4 - pad;
            while (pad-- > 0)
                packet[index++] = 0;
        }
        return index;
    }

    private int EncodeSLIP(byte[] packet, int length)
    {
        /// The End Of Transmission character for arduino
        byte eot = 0xc0;
        byte slipesc = 0xdb;
        byte slipescend = 0xdc;
        byte slipescesc = 0xdd;

        byte[] tmpPacket = new byte[1024];
        packet.CopyTo(tmpPacket, 0);

        packet[0] = eot;

        int index = 0;
        int tmpIndex = 0;

        while (index <= 1022)
        {

            index++;

            if (tmpPacket[tmpIndex] == eot)
            {
                packet[index++] = slipesc;
                packet[index] = slipescend;
            }
            else if (tmpPacket[tmpIndex] == slipesc)
            {
                packet[index++] = slipesc;
                packet[index] = slipescesc;
            }
            else
            {
                packet[index] = tmpPacket[tmpIndex];
            }

            tmpIndex++;
        }

        index = length + 1 + (index - tmpIndex);

        packet[index++] = eot;
        return index;
    }


    /// <summary>
    /// Takes a length and returns what it would be if padded to the nearest 4 bytes.
    /// </summary>
    /// <param name="rawSize">Original size</param>
    /// <returns>padded size</returns>
    private int PadSize(int rawSize)
    {
        int pad = rawSize % 4;
        if (pad == 0)
            return rawSize;
        else
            return rawSize + (4 - pad);
    }

}

/// <summary>
/// The OscMessage class is a data structure that represents
/// an OSC address and an arbitrary number of values to be sent to that address.
/// </summary>
public class OscMessage
{
    /// <summary>
    /// The OSC address of the message as a string.
    /// </summary>
    public string address;

    /// <summary>
    /// The list of values to be delivered to the Address.
    /// </summary>
    public ArrayList values;

    public OscMessage()
    {
        values = new ArrayList();
    }

    public override string ToString()
    {
        StringBuilder s = new StringBuilder();
        s.Append(address);
        foreach (object o in values)
        {
            s.Append(" ");
            s.Append(o.ToString());
        }
        return s.ToString();
    }

    public int GetInt(int index)
    {

        if (values[index].GetType() == typeof(int))
        {
            int data = (int)values[index];
            if (Double.IsNaN(data)) return 0;
            return data;
        }
        else if (values[index].GetType() == typeof(float))
        {
            int data = (int)((float)values[index]);
            if (Double.IsNaN(data)) return 0;
            return data;
        }
        else
        {
            Debug.Log("Wrong type");
            return 0;
        }
    }

    public float GetFloat(int index)
    {

        if (values[index].GetType() == typeof(int))
        {
            float data = (int)values[index];
            if (Double.IsNaN(data)) return 0f;
            return data;
        }
        else if (values[index].GetType() == typeof(float))
        {
            float data = (float)values[index];
            if (Double.IsNaN(data)) return 0f;
            return data;
        }
        else
        {
            Debug.Log("Wrong type");
            return 0f;
        }
    }

}

public delegate void OscMessageHandler(OscMessage oscM);

