﻿/**
 * Ardity (Serial Communication for Arduino + Unity)
 * Author: Daniel Wilches <dwilches@gmail.com>
 *
 * This work is released under the Creative Commons Attributions license.
 * https://creativecommons.org/licenses/by/2.0/
 * 
 * Modified to implement basid OSC protocol <telenacoB@gmail.com>
 *
 */

using UnityEngine;
using System.Threading;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Text;
using System;


public class SerialControllerOsc : MonoBehaviour
{
    [Tooltip("Port name with which the SerialPort object will be created.")]
    public string portName = "COM3";

    [Tooltip("Baud rate that the serial device is using to transmit data.")]
    public int baudRate = 115200;

    // [Tooltip("Reference to an scene object that will receive the events of connection, " +
    //          "disconnection and the messages from the serial device.")]
    //public GameObject messageListener;

    [Tooltip("After an error in the serial communication, or an unsuccessful " +
             "connect, how many milliseconds we should wait.")]
    public int reconnectionDelay = 500;

    // Constants used to mark the start and end of a connection. There is no
    // way you can generate clashing messages from your serial device, as I
    // compare the references of these strings, no their contents. So if you
    // send these same strings from the serial device, upon reconstruction they
    // will have different reference ids.
    public const string SERIAL_DEVICE_CONNECTED = "__Connected__";
    public const string SERIAL_DEVICE_DISCONNECTED = "__Disconnected__";

    // Internal reference to the Thread and the object that runs in it.
    protected Thread thread;
    protected SerialThreadOsc serialThreadOsc;

    // ------------------------------------------------------------------------
    // Invoked whenever the SerialController gameobject is activated.
    // It creates a new thread that tries to connect to the serial device
    // and start reading from it.
    // ------------------------------------------------------------------------
    void OnEnable()
    {
        serialThreadOsc = new SerialThreadOsc(portName,
                                              baudRate,
                                              reconnectionDelay);
        thread = new Thread(new ThreadStart(serialThreadOsc.RunForever));
        thread.Start();

        // when message received with the address /data, execute ReceiveOsc
        serialThreadOsc.SetAddressHandler("/data", ReceiveOsc);
    }

    //----------------------------------------------------------------------
    // Puts a message in the outgoing queue. The thread object will send the
    // message to the serial device when it considers it's appropriate.
    // ----------------------------------------------------------------------
    public void SendOSCMessage(OscMessage oscM)
    {
        serialThreadOsc.SendMessage(oscM);
    }

    void ReceiveOsc(OscMessage _msg)
    {
        Debug.Log(_msg);
    }

    public void SetAddress(string key, OscMessageHandler ah)
    {
        serialThreadOsc.SetAddressHandler(key, ah);
    }

    // ------------------------------------------------------------------------
    // Invoked whenever the SerialController gameobject is deactivated.
    // It stops and destroys the thread that was reading from the serial device.
    // ------------------------------------------------------------------------
    void OnDisable()
    {
        // The serialThreadOsc reference should never be null at this point,
        // unless an Exception happened in the OnEnable(), in which case I've
        // no idea what face Unity will make.
        if (serialThreadOsc != null)
        {
            serialThreadOsc.RequestStop();
            serialThreadOsc = null;
        }

        // This reference shouldn't be null at this point anyway.
        if (thread != null)
        {
            thread.Join();
            thread = null;
        }
    }
}