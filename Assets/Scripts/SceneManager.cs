﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// data structure to send data to the controller
/// </summary>
public class DataPackage
{
    public Vector3 OutputTorque;
    public Vector3 DiskRotation;
    public float gimbalSpeed = 0.0f;
}

/// <summary>
/// Serial data control and scene components
/// </summary>
public class SceneManager : MonoBehaviour
{

    // objects that we have in the scene at any giving time

    public SerialControllerOsc osc;
    public Transform controller;
    public GameObject ball;
    public GameObject Cannon;

    GameObject _ball;

    // start/stop the cannon from the editor
    public bool SpawningBalls = true;
    public bool moveCannon = true;
    public bool ClosestDebugLine = false;
    [Range(0.0f, 10.0f)]
    public float CannonRotVel = 2.0f;

    // for debugging
    Color DebugArrowCol = Color.blue;


    #region Singleton 
    //Singleton pattern
    protected static SceneManager _instance = null;
    protected SceneManager()
    {
        //Get access to all required nodes in the scene;
    }
    public static SceneManager instance()
    {
        return _instance;
    }
    #endregion

    #region UNITY
    void Start()
    {
        // instantiate singleton
        _instance = this;

        // serial com address, receive echo
        // osc = GetComponent<SerialControllerOsc>();

        // place holder from where the balls are launch
        Cannon.transform.position = new Vector3(0.0f, 1.0f, -8.0f);
        Cannon.transform.LookAt(controller);

        // // if we are receiveing messages
        // osc.SetAddress("/data", oscReceived);

        //StartCoroutine(ShootBalls());

    }

    private void Update()
    {
        rotateCannon();

        if (Input.GetKeyDown("space"))
        {
            _ball = Instantiate(ball, Cannon.transform.position, Cannon.transform.rotation);
        }
    }
    #endregion

    public IEnumerator ShootBalls()
    {
        while (SpawningBalls)
        {
            yield return new WaitForSeconds(15);
            // script in the tennis ball launches it to the controller and destroys it. 
            _ball = Instantiate(ball, Cannon.transform.position, Cannon.transform.rotation);
        }
    }

    void rotateCannon()
    {
        if (moveCannon)
        {
            Cannon.transform.LookAt(controller.position, controller.up);
            // Rotates the transform about axis passing through point in world coordinates by angle degrees.
            Cannon.transform.RotateAround(controller.position, Vector3.up, CannonRotVel * Time.deltaTime);
            Cannon.transform.RotateAround(controller.position, Vector3.right, CannonRotVel * Time.deltaTime);
            //Cannon.transform.RotateAround(Paddle.transform.position, Vector3.forward, CannonRotVel  * Time.deltaTime);
        }
    }

    #region COM methods
    // public void msgData(Vector3 _nextPos) {
    //     OscMessage message = new OscMessage();
    //     message.address = "/data";
    //     message.values.Add(_nextPos.x);
    //     message.values.Add(_nextPos.y);
    //     message.values.Add(_nextPos.z);

    //     osc.SendOSCMessage(message);
    // }

    // void oscReceived(OscMessage _msg) {
    //     //recive echo message if any
    //     Debug.Log(_msg);
    // }

    #endregion
}
