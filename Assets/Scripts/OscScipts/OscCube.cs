﻿using UnityEngine;

public class OscCube : MonoBehaviour {

    [SerializeField]
    private AudioSource _audioSource;


    private Color newColor = new Color(0.0f, 0.0f, 0.0f);
    private Vector2 XYposition;
    private GameObject _light;

    public void OnEnable()
    {
        _light = transform.Find("Point Light").gameObject;
        if (_light == null) Debug.Log("not light found");


    }

    public void UpdateRed(float r)
    {
        newColor.r = r;
        gameObject.GetComponent<Renderer>().material.color = newColor;
    }

    public void UpdateGreen(float g)
    {
        newColor.g = g;
        gameObject.GetComponent<Renderer>().material.color = newColor;
    }

    public void UpdateBlue(float b)
    {
        newColor.b = b;
        gameObject.GetComponent<Renderer>().material.color = newColor;
    }

    // there is no OSC implementation for receiving bool
    public void DisableLight(int val)
    {
        if (val == 0) _light.SetActive(false);
        else if (val == 1) _light.SetActive(true);
    }

    public void UpdatePosition(Vector2 position)
    {
        // z value remain the same when updating the position, x and y are update based on the recieved xy message
        Vector3 pos = gameObject.GetComponent<Transform>().transform.parent.position;
        gameObject.GetComponent<Transform>().transform.position = new Vector3((pos.x + position.x * 2), (pos.y + position.y * 2), pos.z);
    }

    public void PlayTrack(int val)
    {
        if (val == 0 && !_audioSource.isPlaying) _audioSource.Play();
        else if (val == 1 && _audioSource.isPlaying) _audioSource.Stop();
    }
}
