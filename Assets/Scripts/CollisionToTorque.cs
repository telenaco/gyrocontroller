﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionToTorque : MonoBehaviour
{
    public Color color = Color.white;
    Vector3 angularMomentum;

    private void OnCollisionEnter(Collision collision)
    {
        Vector3 colImpulse = collision.impulse;

        DrawArrow(collision.contacts[0].point, colImpulse, 0.2f, color, 5, false);
        Debug.Log("output collision impulses " + colImpulse.x + " - " + colImpulse.y + " - " + colImpulse.z);
        Debug.Log( colImpulse.magnitude.ToString("F4"));

        Vector3 one = GetComponent<Rigidbody>().centerOfMass;
        Vector3 two = collision.impulse;
        Vector3 three = collision.transform.position;


    }



    private void DrawArrow(Vector3 position, Vector3 direction, float arrowSize, Color color, float duration = 0, bool depthTest = false)
    {
        Vector3 cross = Vector3.Cross(direction, Vector3.up).normalized;

        if (cross.magnitude < Mathf.Epsilon)
            cross = Vector3.Cross(direction, Vector3.right).normalized;

        Vector3 upwardsCross = Vector3.Cross(direction, cross).normalized;

        Vector3 norm = direction.normalized;

        Vector3 tip = position + direction;

        Vector3 p1 = tip + cross * arrowSize * 0.4f - norm * arrowSize;
        Vector3 p2 = tip - cross * arrowSize * 0.4f - norm * arrowSize;
        Vector3 p3 = tip + upwardsCross * arrowSize * 0.4f - norm * arrowSize;
        Vector3 p4 = tip - upwardsCross * arrowSize * 0.4f - norm * arrowSize;

        Debug.DrawRay(position, direction, color, duration, depthTest);
        Debug.DrawRay(tip, p1 - tip, color, duration, depthTest);
        Debug.DrawRay(tip, p2 - tip, color, duration, depthTest);
        Debug.DrawRay(tip, p3 - tip, color, duration, depthTest);
        Debug.DrawRay(tip, p4 - tip, color, duration, depthTest);
    }


}
