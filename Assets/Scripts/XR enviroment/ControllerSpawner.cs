﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.XR;
using System.Globalization;

public class ControllerSpawner : MonoBehaviour {

    public bool ShowController = false;
    public InputDeviceCharacteristics ControllerCharacteristics;
    public List<GameObject> ControllerPrefabs;
    public GameObject HandModelPrefab;

    private InputDevice _targetDevice;
    private GameObject _spawnedController;
    private GameObject _spawnedHandModel;
    private Animator _handAnimator;

    void Start()
    {
        TryInitialize();
    }

    void TryInitialize()
    {
        // find present controllers
        List<InputDevice> devices = new List<InputDevice>();

        InputDevices.GetDevicesWithCharacteristics(ControllerCharacteristics, devices);

        // list of present devices
        //foreach (var item in devices)
        //{
        //    Debug.Log("total number of devices found is _>>> " + devices.Count);
        //    // print on console all the devices found
        //    Debug.Log(string.Format("Device found with name '{0}' and role '{1}' and characteristics '{2}'", item.name, item.role.ToString(), item.characteristics));

        //}

        // the first controller that match the characteristics will be the one that we instantiate the controller for
        if (devices.Count > 0)
        {
            _targetDevice = devices.FirstOrDefault(); 
            // trackers include the serial number as part of the name, remove it
            string[] delimiter = { " S/N " }; ;
            string[] splitName = _targetDevice.name.Split(new string[] { " S/N " }, StringSplitOptions.None);
            GameObject prefab = ControllerPrefabs.Find(controller => controller.name == splitName[0]);
            if (prefab)
            {
                _spawnedController = Instantiate(prefab, transform);
            }
            else
            {
                Debug.Log("Did not find corresponding controller model for -> " + splitName[0] + " <- ");
                // ControllerPrefabs[0] is the default controller
                _spawnedController = Instantiate(ControllerPrefabs[0], transform);
            }
            _spawnedHandModel = Instantiate(HandModelPrefab, transform);
            _handAnimator = _spawnedHandModel.GetComponent<Animator>();
        }
    }

    void UpdateHandAnimation()
    {
        // CommonUsages returns the float reading of the trigger or grip used for the anymation
        if (_targetDevice.TryGetFeatureValue(CommonUsages.trigger, out float triggerValue)) _handAnimator.SetFloat("Trigger", triggerValue);
        else _handAnimator.SetFloat("Trigger", 0);

        if (_targetDevice.TryGetFeatureValue(CommonUsages.grip, out float gripValue)) _handAnimator.SetFloat("Grip", gripValue);
        else _handAnimator.SetFloat("Grip", 0);
    }

    // Update is called once per frame
    void Update()
    {

        // if controller not init on start keep checking in case the controller is activated after start
        if (!_targetDevice.isValid)
        {
            TryInitialize();
        }
        else
        {
            if (ShowController)
            {
                _spawnedHandModel.SetActive(false);
                _spawnedController.SetActive(true);
            }
            else
            {
                // if we are using the hands instead of controller  update the animation based on the trigger action
                _spawnedHandModel.SetActive(true);
                _spawnedController.SetActive(false);
                UpdateHandAnimation();
            }
        }
    }
}
