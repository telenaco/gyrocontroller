﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class LocomotionController : MonoBehaviour {

    public XRController LeftTeleportRay;
    public XRController RightTeleportRay;
    public InputHelpers.Button TeleportActivationButton;
    public float ActivationThreshold = 0.1f;

    public XRRayInteractor LeftRayInteractor;
    public XRRayInteractor RightRayInteractor;

    public bool EnableLeftTeleport { get; set; } = true;
    public bool EnableRightTeleport { get; set; } = true;

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = new Vector3();
        Vector3 norm = new Vector3();
        int index = 0;
        bool validTarget = false;


        if (RightTeleportRay)
        {
            bool isRightInteractorHovering =
                RightRayInteractor.TryGetHitInfo(out pos, out norm, out index, out validTarget);

            RightTeleportRay.gameObject.SetActive(!isRightInteractorHovering && EnableRightTeleport && CheckIfActivated(RightTeleportRay));
        }

        if (LeftTeleportRay)
        {
            bool isLeftInteractorHovering =
                LeftRayInteractor.TryGetHitInfo(out pos, out norm, out index, out validTarget);

            LeftTeleportRay.gameObject.SetActive(!isLeftInteractorHovering && EnableLeftTeleport && CheckIfActivated(LeftTeleportRay));
        }

    }

    public bool CheckIfActivated(XRController controller)
    {
        // use events to get the pressed button 
        InputHelpers.IsPressed(controller.inputDevice, TeleportActivationButton, out bool isActivated, ActivationThreshold);
        return isActivated;
    }
}