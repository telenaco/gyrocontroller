﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public class ContinuousMovement : MonoBehaviour
{
    public float Speed = 1;
    public XRNode InputSource;
    public float Gravity = -9.81f;
    public LayerMask GroundLayer;
    public float AdditionalHeight = 0.2f;

    private float _fallingSpeed;
    private XRRig _rig;
    private Vector2 _inputAxis;
    private CharacterController _character;

    void Start()
    {
        _character = GetComponent<CharacterController>();
        _rig = GetComponent<XRRig>();
    }

    void Update()
    {
        InputDevice device = InputDevices.GetDeviceAtXRNode(InputSource);
        // capture the trackpad reading 
        device.TryGetFeatureValue(CommonUsages.primary2DAxis, out _inputAxis);
    }

    private void FixedUpdate()
    {
        // update current position
        CapsuleFollowHeadset();

        Quaternion headYaw = Quaternion.Euler(0, _rig.cameraGameObject.transform.eulerAngles.y, 0);
        // set the new direction that the character is moving 
        Vector3 direction = headYaw *  new Vector3(_inputAxis.x, 0, _inputAxis.y);

        // move the character to ne new position
        _character.Move(direction * Time.fixedDeltaTime * Speed);

        // fall if no plane underneath 
        bool isGrounded = CheckIfGrounded();
        if (isGrounded)
            _fallingSpeed = 0;
        else
            // calculate falling speed, increments over time
            _fallingSpeed += Gravity * Time.fixedDeltaTime;

        _character.Move(Vector3.up * _fallingSpeed * Time.fixedDeltaTime);
    }

    //user collider, collider moves when using the trackpad but no when user moves on space. Below moves capsule
    void CapsuleFollowHeadset()
    {
        _character.height = _rig.cameraInRigSpaceHeight + AdditionalHeight;
        // get local coordinates 
        Vector3 capsuleCenter = transform.InverseTransformPoint(_rig.cameraGameObject.transform.position);
        _character.center = new Vector3(capsuleCenter.x, _character.height/2 + _character.skinWidth , capsuleCenter.z);
    }

    bool CheckIfGrounded()
    {
        //tells us if on ground
        Vector3 rayStart = transform.TransformPoint(_character.center);
        float rayLength = _character.center.y + 0.01f;
        bool hasHit = Physics.SphereCast(rayStart, _character.radius, Vector3.down, out RaycastHit hitInfo, 
            rayLength, GroundLayer);
        return hasHit;
    }
}
