﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DomePosiiton : MonoBehaviour
{
    public GameObject cannon;
    public GameObject paddle;

    public Vector3 cannonDis;

    public bool stopSpawning = false;
    public float spawnTime;
    public float spawnDelay;

    Vector3 paddlePos;
    // Quaternion paddleRot;
    Transform paddleRot;

    // Start is called before the first frame update
    void Start()
    {
        //paddleRot = paddle.transform.rotation;
        paddlePos = paddle.transform.position;
    }

    void Update()
    {
        float xPos = paddlePos.x + cannonDis.x * Mathf.Cos(Random.Range(0, 360));
        float yPos = paddlePos.y + cannonDis.y * Mathf.Sin(Random.Range(0, 360));
        float zPos = 0.0f;

        Vector3 cannonPos = new Vector3(xPos, yPos, zPos);
        Instantiate(cannon, cannonPos, Quaternion.identity);
        cannon.transform.LookAt(paddle.transform);
        StartCoroutine(destroyAfterTime(2));
    }

    IEnumerator destroyAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        Destroy(this.gameObject);
    }
}






// public GameObject projectile;

// public float launchVelocity = 700f;

// public bool stopSpawning = false;
// public float spawnTime;
// public float spawnDelay;
// public float prevMillis;
// public float currMillis;

// // Start is called before the first frame update
// void Start()
// {
//     //InvokeRepeating("SpawnObject", spawnTime, spawnDelay);
//     GameObject ball = Instantiate(projectile, transform.position,
//                                       transform.rotation);

//     ball.GetComponent<Rigidbody>().AddRelativeForce(new Vector3
//                                          (0, launchVelocity, 0));
// }

// // void OnTriggerEnter(Collider coll)
// // {
// //     coll.gameObject.GetComponent<Rigidbody2D>().gravity = 0f; //disable gravity on the collided object
// //     this.GetComponent<Rigidbody2D>().gravity = 0f; //disable gravity on the objected collided into (IE, this one)
// // }

// // Update is called once per frame
// void Update()
// {

//     // currMillis = Time.time *1000;

//     // if ((currMillis - prevMillis) >= spawnTime)
//     // {
//     //     prevMillis = currMillis;
//     //     GameObject ball = Instantiate(projectile, transform.position,
//     //                                               transform.rotation);

//     //     ball.GetComponent<Rigidbody>().AddRelativeForce(new Vector3
//     //                                          (0, launchVelocity, 0));
//     // }

//     // Instantiate(spawnee, transform.position, transform.rotation);
//     // if (stopSpawning)
//     // {
//     //     CancelInvoke("SpawnObject");
//     // }

// }
// }
