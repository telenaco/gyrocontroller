﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public struct PositionRotationTorque
{
    Vector3 position;
    Quaternion rotation;
    public Vector3 torque;

    public PositionRotationTorque(Vector3 position, Quaternion rotation, Vector3 torque)
    {
        this.position = position;
        this.rotation = rotation;
        this.torque = torque;
    }

    // PositionRotationTorque to Vector3
    public static implicit operator Vector3(PositionRotationTorque pr)
    {
        return pr.position;
    }
    // PositionRotationTorque to Quaternion
    public static implicit operator Quaternion(PositionRotationTorque pr)
    {
        return pr.rotation;
    }
}

public class GyroActuation : MonoBehaviour
{
    [Header("UPDATE POSITION")]
    public bool updatePos = true;

    [Header("Joints")]
    public Transform BaseJoint;
    //[HideInInspector]
    [ReadOnly]
    public AxelJoint[] Joints = null;
    // The current angles
    [ReadOnly]
    public float[] Solution = null;

    [Header("Destination")]
    public Transform lastJoint;
    [Space]
    private Quaternion target;

    [Header("Inverse Kinematics")]
    [Range(0, 100.0f)]
    public float DeltaGradient = 0.01f; // Used to simulate gradient (degrees)
    [Range(0, 10.0f)]
    public float LearningRate = 0.01f; // How much we move depending on the gradient

    [Space()]
    [Range(0, 5.0f)]
    public float StopThreshold = 0.1f; // If closer than this, it stops
    [Range(0, 10.0f)]
    public float SlowdownThreshold = 0.25f; // If closer than this, it linearly slows down

    [Header("Debug")]
    public bool DebugDraw = true;

    GyroController GyC;
    Vector3 targetTorque;

    void Start()
    {
        GyC = GyroController.instance();

        if (Joints == null)
            GetJoints();
    }

    [ExposeInEditor(RuntimeOnly = false)]
    public void GetJoints()
    {
        Joints = BaseJoint.GetComponentsInChildren<AxelJoint>();
        Solution = new float[Joints.Length];
    }

    // Update is called once per frame
    void Update()
    {

        ForwardKinematicsTorque(Solution);

        // if the collider list is null, empty of the closest object was deleted
        if (GyC.collisionControl.CollidersList == null ||
            GyC.collisionControl.CollidersList.Count == 0 ||
            GyC.collisionControl.ClosestObject == null)
            return;


        Vector3 targetPos = GyC.collisionControl.ClosestObject.transform.position;
        // calculate direction to the ball
        Vector3 direction = (targetPos - lastJoint.transform.position).normalized;
        // calculate the output torque with a collision from that direction
        targetTorque = GyC.gyroModeling.calcTorque(direction);

        Debug.DrawRay(transform.position, targetTorque.normalized, Color.white);

        // if the simlated torque does not align keep moving it
        if (RotationFromTarget(targetTorque, Solution) > StopThreshold)
            RotateToTarget(targetTorque);

        if (DebugDraw)
        {
            Debug.DrawLine(lastJoint.transform.position, targetPos, Color.white);
        }
    }

    public void RotateToTarget(Vector3 _targetTorque)
    {
        // Starts from the flywheel, up to the handle
        // for (int i = Joints.Length - 1; i >= 0; i--)
        for (int i = 0; i < Joints.Length - 1; i++)
        {
            // FROM:    error:      [0, StopThreshold,  SlowdownThreshold]
            // TO:      slowdown:   [0, 0,              1                ]
            float error = RotationFromTarget(_targetTorque, Solution);
            // multiply the slowdown by the gradient to "slow" aproach
            float slowdown = Mathf.Clamp01((error - StopThreshold) / (SlowdownThreshold - StopThreshold));

            // Gradient descent
            float gradient = CalculateGradient(_targetTorque, Solution, i, DeltaGradient);
            Solution[i] -= LearningRate * gradient * slowdown;
            // Clamp
            // Solution[i] = Joints[i].ClampAngle(Solution[i]);

            // Early termination
            if (RotationFromTarget(_targetTorque, Solution) <= StopThreshold)
                break;
        }

        for (int i = 0; i < Joints.Length - 1; i++)
        {
            // convert the angles from unity CF to controller CF before moving it??
            Joints[i].SetRadians(Solution[i]);
        }
        Vector3 tmp1 = new Vector3(0.0f, Solution[0], Solution[1]);

        Debug.DrawRay(transform.position, forwardModel(tmp1).normalized, Color.black, 1.2f);
    }

    /* Calculates the gradient for the invetse kinematic.
     * It simulates the forward kinematics the i-th joint,
     * by moving it +delta and -delta.
     * It then sees which one gets closer to the target.
     * It returns the gradient (suggested changes for the i-th joint)
     * to approach the target. In range (-1,+1)
     */
    public float CalculateGradient(Vector3 _targetTorque, float[] Solution, int i, float delta)
    {
        // Saves the angle,
        // it will be restored later
        float solutionAngle = Solution[i];

        // Gradient : [F(x+h) - F(x)] / h
        // Update   : Solution -= LearningRate * Gradient
        float f_x = RotationFromTarget(_targetTorque, Solution);

        Solution[i] += delta;
        float f_x_plus_h = RotationFromTarget(_targetTorque, Solution);

        float gradient = (f_x_plus_h - f_x) / delta;

        // Restores
        Solution[i] = solutionAngle;

        return gradient;
    }

    // Returns the distance from the target, given a solution rotatoin on angles
    public float RotationFromTarget(Vector3 _targetTorque, float[] Solution)
    {
        Vector3 modelTorque = ForwardKinematicsTorque(Solution);

        float MRadius, MAzimut, MElevation;
        float SRadius, SAzimut, SElevation;
        GyC.helperMethods.CartesianToSpherical(modelTorque.normalized, out MRadius, out MAzimut, out MElevation);
        GyC.helperMethods.CartesianToSpherical(_targetTorque.normalized, out SRadius, out SAzimut, out SElevation);

        float distance;
        float tmp1 = Mathf.Abs(Mathf.DeltaAngle(MAzimut * Mathf.Rad2Deg, SAzimut * Mathf.Rad2Deg));
        float tmp2 = Mathf.Abs(Mathf.DeltaAngle(MElevation * Mathf.Rad2Deg, SElevation * Mathf.Rad2Deg));
        distance = tmp1 + tmp2;
        //distance = Vector3.Dot(modelTorque.normalized, _targetTorque.normalized);
        //Debug.Log("distance from model to simulated torque -> " + distance);
        return distance;
    }

    /* Simulates the forward kinematics,
     * given an increment. */
    public Vector3 ForwardKinematicsTorque(float[] Solution)
    {
        // rotation on z on the controller coordinates is yaw
        // rotation on y on the controller coordinates is pitch
        Vector3 angleRotation;
        angleRotation.x = 0;
        angleRotation.y = GyC.PitchAxle.Rot ;
        angleRotation.z = GyC.YawAxle.Rot ;

        Vector3 tmp = forwardModel(angleRotation);
        // Axis has moved to the next position, calculate output torque
        return tmp;
    }

    Vector3 forwardModel(Vector3 rotation)
    {
        // CONTROLLER CF

        //GyC.helperMethods.unityToController(rotation);
        Vector3 localModelTorque;
        Vector3 outModelTorque;

        // Given a rotation calculate the output torque direction 
        float PitchRot = rotation.y;
        float YawRot = rotation.z;
        Debug.Log(" yaw -> " + rotation.z + " pitch -> " + rotation.y);
 
        float PitchVel = 0.01f;
        float PitchAcc = 0.01f;
        float YawAcc = 0.1f;
        float YawVel = 0.1f;

        // Compute the model 
        localModelTorque.x = (0.25f * GyC.Inertia) *
            ((2.0f * PitchVel * GyC.diskAngVel) -
            (YawAcc * Mathf.Sin(GyC.PitchAxle.Rot)));

        localModelTorque.y = (0.25f * GyC.Inertia) *
            (PitchAcc +
            ((YawVel * Mathf.Sin(GyC.PitchAxle.Rot) *
            ((2.0f * GyC.diskAngVel) + (YawVel * Mathf.Cos(GyC.PitchAxle.Rot))))));

        localModelTorque.z = (0.5f * GyC.Inertia) *
            ((YawAcc * Mathf.Cos(GyC.PitchAxle.Rot)) -
            (PitchVel * YawVel * Mathf.Sin(GyC.PitchAxle.Rot)));

        // Rotate it to handle 
        Vector4 inputVector = new Vector4(localModelTorque.x, localModelTorque.y, localModelTorque.z, 0.0f);

        Matrix4x4 rotMat = Matrix4x4.zero;
        rotMat.m00 = Mathf.Cos(PitchRot) * Mathf.Cos(YawRot);
        rotMat.m01 = -Mathf.Sin(YawRot);
        rotMat.m02 = Mathf.Sin(PitchRot) * Mathf.Cos(YawRot);
        rotMat.m10 = Mathf.Cos(PitchRot) * Mathf.Sin(YawRot);
        rotMat.m11 = Mathf.Cos(YawRot);
        rotMat.m12 = Mathf.Sin(PitchRot) * Mathf.Sin(YawRot);
        rotMat.m20 = Mathf.Sin(PitchRot);
        rotMat.m22 = Mathf.Cos(PitchRot);

        Vector4 tmp = rotMat * inputVector;
        outModelTorque = new Vector3(tmp.x, tmp.y, tmp.z);
        // outModel represents torque on hand coordinates now
        // change it to unity CF
        outModelTorque = GyC.helperMethods.unityToController(outModelTorque);
        // change it to world coordinates
        Quaternion rot = GyC.transform.rotation;
        outModelTorque = rot * outModelTorque;

        return outModelTorque;
    }
}
