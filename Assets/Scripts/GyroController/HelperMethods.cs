﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelperMethods : MonoBehaviour
{
    // matrix used to rotate from center of flywheel to handle of the controller using the controller coordinate system
    Matrix4x4 rotMat = Matrix4x4.zero;

    void updateRotMat()
    {

        // update yaw and pitch rotation values and apply it to the rotation matrix
        float PitchRot = GyroController.instance().PitchAxle.Rot;
        float YawRot = GyroController.instance().YawAxle.Rot;

        rotMat.m00 = Mathf.Cos(PitchRot) * Mathf.Cos(YawRot);
        rotMat.m01 = -Mathf.Sin(YawRot);
        rotMat.m02 = Mathf.Sin(PitchRot) * Mathf.Cos(YawRot);
        rotMat.m10 = Mathf.Cos(PitchRot) * Mathf.Sin(YawRot);
        rotMat.m11 = Mathf.Cos(YawRot);
        rotMat.m12 = Mathf.Sin(PitchRot) * Mathf.Sin(YawRot);
        rotMat.m20 = Mathf.Sin(PitchRot);
        rotMat.m22 = Mathf.Cos(PitchRot);
    }

    // flywheel to handle
    public Vector3 rotateA_to_D(Vector3 _input)
    {
        return rotFlywheelToHandle(_input, true);
    }

    // flywheel to handle
    public Quaternion rotateA_to_D(Quaternion _input)
    {
        return rotFlywheelToHandle(_input, true);
    }

    // handle to flywheel
    public Vector3 rotateD_to_A(Vector3 _input)
    {
        return rotFlywheelToHandle(_input, false);
    }

    // handle to flywheel
    public Quaternion rotateD_to_A(Quaternion _input)
    {
        return rotFlywheelToHandle(_input, false);
    }

    /// <summary>
    /// rotates vector from handle to flywheel coordinates
    /// </summary>
    /// <param name="_input"></param> vector to rotate
    /// <param name="direction"></param> true for flywheel to handle, false to handle ot flywheel
    /// <returns></returns>
    public Vector3 rotFlywheelToHandle(Vector3 _input, bool direction)
    {
        Vector4 inputRotation = new Vector4(_input.x, _input.y, _input.z, 0.0f);

        updateRotMat();

        Vector4 tOut = Vector4.zero;

        if (direction)
            tOut = rotMat * inputRotation;
        else if (!direction)
            tOut = rotMat.inverse * inputRotation;

        return new Vector3(tOut.x, tOut.y, tOut.z);
    }

    /// <summary>
    /// rotates quaternion by a 4x4 rotation matrix  from handle to flywheel coordinates 
    /// </summary>
    /// <param name="_input"></param> vector to rotate
    /// <param name="direction"></param> true for flywheel to handle, false to handle ot flywheel
    /// <returns></returns>
    public Quaternion rotFlywheelToHandle(Quaternion _input, bool direction)
    {
        updateRotMat();

        // Adapted from: http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/index.htm
        Quaternion q = new Quaternion();
        q.w = Mathf.Sqrt(Mathf.Max(0, 1 + rotMat[0, 0] + rotMat[1, 1] + rotMat[2, 2])) / 2;
        q.x = Mathf.Sqrt(Mathf.Max(0, 1 + rotMat[0, 0] - rotMat[1, 1] - rotMat[2, 2])) / 2;
        q.y = Mathf.Sqrt(Mathf.Max(0, 1 - rotMat[0, 0] + rotMat[1, 1] - rotMat[2, 2])) / 2;
        q.z = Mathf.Sqrt(Mathf.Max(0, 1 - rotMat[0, 0] - rotMat[1, 1] + rotMat[2, 2])) / 2;
        q.x *= Mathf.Sign(q.x * (rotMat[2, 1] - rotMat[1, 2]));
        q.y *= Mathf.Sign(q.y * (rotMat[0, 2] - rotMat[2, 0]));
        q.z *= Mathf.Sign(q.z * (rotMat[1, 0] - rotMat[0, 1]));

        if (direction)
            _input *= q;
        else if (!direction)
            _input *= Quaternion.Inverse(q);

        return _input;
    }

    // change a rotation from rightHC to leftHC 
    public Quaternion unityToController(Quaternion _inputQuaternion)
    {
        return new Quaternion(-_inputQuaternion.x,
                                -_inputQuaternion.z,
                                -_inputQuaternion.y,
                                _inputQuaternion.w);
    }

    public Vector3 unityToController(Vector3 _inputVector)
    {
        return new Vector3(_inputVector.x, _inputVector.z, _inputVector.y);
    }

    /// <summary>
    /// Converts a point from Spherical coordinates to Cartesian. All angles are in radians.
    /// </summary>
    /// <param name="radius">in radians</param>
    /// <param name="polar">in radians</param>
    /// <param name="elevation">in radians</param>
    /// <param name="outCart"> cartesion angles in radians</param>
    public void SphericalToCartesian(float radius, float polar, float elevation, out Vector3 outCart)
    {
        float a = radius * Mathf.Cos(elevation);
        outCart.x = a * Mathf.Cos(polar);
        outCart.y = radius * Mathf.Sin(elevation);
        outCart.z = a * Mathf.Sin(polar);
    }

    /// <summary>
    /// Converts a point from Cartesian coordinates (using positive Y as up) to spherical and stores the results in the var Radius, Azimuth, Polar
    /// </summary>
    /// <param name="cartCoords">radians</param>
    /// <param name="outRadius">radians</param>
    /// <param name="outAzimut">radians</param>
    /// <param name="outElevation"></param>
    public void CartesianToSpherical(Vector3 cartCoords, out float outRadius, out float outAzimut, out float outElevation)
    {
        if (cartCoords.z == 0)
            cartCoords.z = Mathf.Epsilon;

        outRadius = Mathf.Sqrt((cartCoords.x * cartCoords.x)
                             + (cartCoords.y * cartCoords.y)
                             + (cartCoords.z * cartCoords.z));

        outAzimut = Mathf.Atan(cartCoords.x / cartCoords.z);

        if (cartCoords.z < 0)
            outAzimut += Mathf.PI;

        outElevation = Mathf.Asin(cartCoords.y / outRadius);
    }

    /// <summary>
    /// Draw a debuggin arrow in the unity scene window, does not display on the game window
    /// </summary>
    /// <param name="position"></param> where the arrow starts
    /// <param name="direction"></param> force magnitude 
    /// <param name="arrowSize"></param> size of the arrow
    /// <param name="color"></param> color
    /// <param name="duration"></param> duration before the arrow dissapears 
    /// <param name="depthTest"></param> if the arrow is render in fron of any objects
    /// 
    public void DrawArrow(Vector3 position, Vector3 direction, float arrowSize, Color color, float duration = 0.0f, bool depthTest = false)
    {
        Vector3 cross = Vector3.Cross(direction, Vector3.up).normalized;

        if (cross.magnitude < Mathf.Epsilon)
            cross = Vector3.Cross(direction, Vector3.right).normalized;

        Vector3 upwardsCross = Vector3.Cross(direction, cross).normalized;

        Vector3 norm = direction.normalized;

        Vector3 tip = position + direction;

        Vector3 p1 = tip + cross * arrowSize * 0.4f - norm * arrowSize;
        Vector3 p2 = tip - cross * arrowSize * 0.4f - norm * arrowSize;
        Vector3 p3 = tip + upwardsCross * arrowSize * 0.4f - norm * arrowSize;
        Vector3 p4 = tip - upwardsCross * arrowSize * 0.4f - norm * arrowSize;

        Debug.DrawRay(position, direction, color, duration, depthTest);
        Debug.DrawRay(tip, p1 - tip, color, duration, depthTest);
        Debug.DrawRay(tip, p2 - tip, color, duration, depthTest);
        Debug.DrawRay(tip, p3 - tip, color, duration, depthTest);
        Debug.DrawRay(tip, p4 - tip, color, duration, depthTest);
    }

    /// <summary>
    /// mapping function InMin-InMax -> outMin-outMax
    /// </summary>
    /// <param name="x">input value to map out</param>
    /// <param name="in_min"></param>
    /// <param name="in_max"></param>
    /// <param name="out_min"></param>
    /// <param name="out_max"></param>
    /// <returns>mapped value</returns>
    float map(float x, float in_min, float in_max, float out_min, float out_max)
    {
        return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }
}
