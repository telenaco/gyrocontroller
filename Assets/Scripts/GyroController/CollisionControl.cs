﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionControl : MonoBehaviour
{

    // two main componets on this class: 
    // - list with all the colliders 
    // - closest object to the controller

    private List<GameObject> collidersList = new List<GameObject>();
    public List<GameObject> CollidersList
    {
        set { collidersList = value; }
        get { return collidersList; }
    }

    private GameObject closestObj;
    public GameObject ClosestObject{
        set { closestObj = value; }
        get { return closestObj; }
    }
    
    public void AddCollisionObj(GameObject _obj)
    {
        collidersList.Add(_obj);
    }

    // return closest ballObj to controller
    public void UpdtClosestObj()
    {
        //if no obj do nothing
        if (collidersList.Count == 0) 
            return;

        // initialize to biggest possible number
        float closestDistance = Mathf.Infinity;

        // iterate to find the closest one
        foreach (GameObject _obj in collidersList)
        {
            if (_obj == null)
            {
                collidersList.Remove(_obj);
                return;
            }
            float distanceToTarget = Vector3.Distance(transform.position,_obj.transform.position);

            if (distanceToTarget < closestDistance)
            {
                closestDistance = distanceToTarget;
                closestObj = _obj;
            }
        }
    }

    // remove object from list of collision objs
    void OnCollisionEnter(Collision coll)
    {
        collidersList.RemoveAll(x => x.GetInstanceID() == coll.gameObject.GetInstanceID());
    }
}
