﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class gyroUI : MonoBehaviour {
    public TextMeshProUGUI flywheelText;
    public TextMeshProUGUI gimbalText;
    public TextMeshProUGUI gimbalPidText;
    private bool homing = false;
    private bool rotateGimbals = false;
    private float gimbalSpeed = 0;
    private float gimbalVel;

    private void Start() {
        //StartCoroutine(actuateGimbals());
    }

    public IEnumerator actuateGimbals() {
        while (rotateGimbals) {
            yield return new WaitForSeconds(1);
            OscMessage message = new OscMessage();
            message.address = "/yaw";
            message.values.Add(2.0f);
            SceneManager.instance().osc.SendOSCMessage(message);
            message.address = "/pitch";
            message.values.Add(2.0f);
            SceneManager.instance().osc.SendOSCMessage(message);
        }
    }

    public void updateFlywheelSpeed(float input) {
        float _input = input * 10.0f;
        flywheelText.text = _input.ToString("F0");
        OscMessage message = new OscMessage();
        message.address = "/F_vel";
        message.values.Add(_input);
        SceneManager.instance().osc.SendOSCMessage(message);
    }

    // public void updateGimbalSpeed(float input) {
    //     var _input = input * 5.0f;
    //     gimbalText.text = _input.ToString("F0");
    //     OscMessage message = new OscMessage();
    //     message.address = "/G_vel";
    //     message.values.Add(_input);
    //     SceneManager.instance().osc.SendSerialMessage(message);
    // }

    // public void homeRoutine() {
    //     // OscMessage message = new OscMessage();
    //     // message.address = "/G_vel";
    //     // if (!homing) {
    //     //     // stop updating the gyro position
    //     //     SceneManager.instance().updateGyroPosition = false;
    //     //     // lower gimbal speed to stop it by hand when it reaches position
    //     //     message.values.Add(17.0f);
    //     //     // function that keeps rotating both gimbals 
    //     //     rotateGimbals = true;
    //     //     homing = true;
    //     // } else if (homing) {
    //     //     // restore prev speed
    //     //     message.values.Add(gimbalVel);
    //     //     homing = false;
    //     //     rotateGimbals = false;
    //     //     SceneManager.instance().updateGyroPosition = true;
    //     // }
    //     // SceneManager.instance().osc.SendSerialMessage(message);
    // }

    // public void autoUpdatePos(bool state) {
    //     //SceneManager.instance().updateGyroPosition = state;
    // }

    // public void incrementPitch(float increment) {
    //     OscMessage message = new OscMessage();
    //     message.address = "/pitchI";
    //     message.values.Add(increment);
    //     SceneManager.instance().osc.SendSerialMessage(message);
    // }

    // public void incrementYaw(float increment) {
    //     OscMessage message = new OscMessage();
    //     message.address = "/yawI";
    //     message.values.Add(increment);
    //     SceneManager.instance().osc.SendSerialMessage(message);
    // }

    // public void setAnglePitch(string increment) {
    //     var input = float.Parse(increment);
    //     OscMessage message = new OscMessage();
    //     message.address = "/pitchS";
    //     message.values.Add(input);
    //     SceneManager.instance().osc.SendSerialMessage(message);
    // }

    // public void setAngleYaw(string increment) {
    //     var input = float.Parse(increment);
    //     OscMessage message = new OscMessage();
    //     message.address = "/yawS";
    //     message.values.Add(input);
    //     SceneManager.instance().osc.SendSerialMessage(message);
    // }

    // public void setGimbalPid(bool input) {

    //     OscMessage message = new OscMessage();
    //     message.address = "/sPID";

    //     if (input) {
    //         message.values.Add(1);
    //         gimbalPidText.text = "Gimbal impact";
    //     } else if (!input) {
    //         message.values.Add(0);
    //         gimbalPidText.text = "Gimbal actuation";
    //     }
    //     SceneManager.instance().osc.SendSerialMessage(message);

    // }

    // public void Actuation() {
    //     OscMessage message = new OscMessage();
    //     message.address = "/yaw";
    //     message.values.Add(90.0f);
    //     SceneManager.instance().osc.SendSerialMessage(message);
    //     message.address = "/pitch";
    //     message.values.Add(90);
    //     SceneManager.instance().osc.SendSerialMessage(message);
    // }
}
