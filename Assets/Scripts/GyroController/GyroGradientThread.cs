﻿// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;

// public class Axle
// {
//     public Vector3 axis;
//     public Vector3 startOffset;
//     public Vector3 position;
//     public Quaternion rotation;

//     public Axle(Vector3 position, Quaternion rotation, Vector3 axis, Vector3 startOffset)
//     {
//         this.axis = axis;
//         this.position = position;
//         this.rotation = rotation;
//     }

//     // Axle to Vector3
//     public static implicit operator Vector3(Axle pr)
//     {
//         return pr.position;
//     }
//     // Axle to Quaternion
//     public static implicit operator Quaternion(Axle pr)
//     {
//         return pr.rotation;
//     }

//     public float RotateAxle(float angle){

//             if (this.axis.x == 1) rotation = Quaternion.Euler(angle, 0, 0);
//             else
//             if (this.axis.y == 1) rotation = Quaternion.Euler(0, angle, 0);
//             else
//             if (this.axis.z == 1) rotation = Quaternion.Euler(0, 0, angle);

//         return angle;
//     }
// }

// public class GyroGradientThread : MonoBehaviour
// {
//     Axle[] Joints = null;
//     // The current angles
//     float[] Solution = null;

//     Transform lastJoint;
//     Vector3 target;

//     //Inverse Kinematics
//     //Range(0, 100f)
//     float DeltaGradient = 0.01f; // Used to simulate gradient (degrees)
//     //Range(0, 10000f)
//     public float LearningRate = 10.0f; // How much we move depending on the gradient

//     // Range(0, 0.25f)
//     public float StopThreshold = 0.1f; // If closer than this, it stops


//     void runOnce()
//     {
//         // declare all axis 
//         // yaw
//         Joints[0] = new Axle(Vector3.zero, Quaternion.identity, Vector3.zero, Vector3.zero);
//         // pitch
//         Joints[1] = new Axle(Vector3.zero, Quaternion.identity, Vector3.zero, Vector3.zero);
//         // flywheel
//         Joints[2] = new Axle(Vector3.zero, Quaternion.identity, Vector3.zero, Vector3.zero);

//         Solution = new float[Joints.Length];

//         // Unity vector with the direction normalize direction of the output torque
//         target = Vector3.zero;
//     }

//     // Update is called once per frame
//     void Update()
//     {
//         runOnce();

//         if (DistanceFromTarget(target, Solution) > StopThreshold)
//             RotateToTarget(target);
//         // else if (DistanceFromTarget(target, Solution) <= StopThreshold)
//             // return rotation of both axis
//             // destroy thread clearout
//     }

//     public void RotateToTarget(Vector3 target)
//     {
//         // Starts from the handle
//         // for (int i = Joints.Length - 1; i >= 0; i--)
//         for (int i = 0; i < Joints.Length - 1; i++)
//         {
//             // FROM:    error:      [0, StopThreshold,  SlowdownThreshold]
//             // TO:      slowdown:   [0, 0,              1                ]
//             float error = DistanceFromTarget(target, Solution);

//             // Gradient descent
//             float gradient = CalculateGradient(target, Solution, i, DeltaGradient);
//             Solution[i] -= LearningRate * gradient;

//             // Early termination
//             if (DistanceFromTarget(target, Solution) <= StopThreshold)
//                 break;

//         }

//         for (int i = 0; i < Joints.Length - 1; i++)
//         {
//             Joints[i].RotateAxle(Solution[i]);
//         }
//     }

//     public float CalculateGradient(Vector3 target, float[] Solution, int i, float delta)
//     {
//         // Saves the angle,
//         // it will be restored later
//         float solutionAngle = Solution[i];

//         // Gradient : [F(x+h) - F(x)] / h
//         // Update   : Solution -= LearningRate * Gradient
//         float f_x = DistanceFromTarget(target, Solution);

//         Solution[i] += delta;
//         float f_x_plus_h = DistanceFromTarget(target, Solution);

//         float gradient = (f_x_plus_h - f_x) / delta;

//         // Restores
//         Solution[i] = solutionAngle;

//         return gradient;
//     }

//     // Returns the error angle between the target torque and current torque
//     public float DistanceFromTarget(Vector3 target, float[] Solution)
//     {
//         Vector3 point = ForwardKinematicsTorque(Solution);
//         return Vector3.Distance(target, point);
//     }

//     /* Simulates the forward kinematics,
//      * given an increment. */
//     public Vector3 ForwardKinematicsTorque(float[] Solution)
//     {
//         Vector3 prevPoint = Joints[0].position;

//         // Take object initial rotation into account
//         Quaternion rotation = transform.rotation;

//         for (int i = 1; i < Joints.Length; i++)
//         {
//             // Rotates around a new axis
//             rotation *= Quaternion.AngleAxis(Solution[i - 1], Joints[i - 1].axis);
//             // if the axles are not align the center of the arm will be at a different point
//             Vector3 nextPoint = prevPoint + rotation * Joints[i].startOffset;

//             prevPoint = nextPoint;
//         }

//         return Vector3.zero;

//         //return new PositionRotation(prevPoint, rotation);
//         // tihs returns a normalize vector with the stimate torque based on the current solution
//     }
// }
