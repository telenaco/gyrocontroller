﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GyroController : MonoBehaviour
{
    // singleton
    protected static GyroController _instance = null;
    protected GyroController()
    {
        //Get access to all required nodes in the scene;
    }
    public static GyroController instance()
    {
        return _instance;
    }

    public AxelJoint YawAxle;
    public AxelJoint PitchAxle;
    public bool brakePos = false;

    [DebugGUIGraph(group: 0, min: -6.3f, max: 6.3f, r: 1, g: 0, b: 0, autoScale: true)]
    float YawRot;
    // [DebugGUIGraph(group: 0, min: -0.1f, max: 0.1f, r: 0, g: 1, b: 0, autoScale: true)]
    // float YawVel;
    // [DebugGUIGraph(group: 0, min: -0.1f, max: 0.1f, r: 0, g: 0, b: 1, autoScale: true)]
    // float YawAcc;

    [DebugGUIGraph(group: 1, min: -6.3f, max: 6.3f, r: 1, g: 0, b: 0, autoScale: true)]
    float PitchRot;
    // [DebugGUIGraph(group: 1, min: -0.1f, max: 0.1f, r: 0, g: 1, b: 0, autoScale: true)]
    // float PitchVel;
    // [DebugGUIGraph(group: 1, min: -0.1f, max: 0.1f, r: 0, g: 0, b: 1, autoScale: true)]
    // float PitchAcc;

    // [DebugGUIGraph(min: -1, max: 1, r: 0, g: 1, b: 0, autoScale: true)]
    // float Rot, Vel, Acc;

    // protected GyroData _prevGyroData = new GyroData(); // store previous state
    // protected GyroData _gyroData = new GyroData();
    // public GyroData gyroData
    // {
    //     set { _gyroData = value; }
    //     get { return gyroData; }
    // }

    float diskMass = 0.096f;                    // 96g
    float radius = 0.050f;                     // 10cm diameter
    [HideInInspector]
    public float diskAngVel = 753.0f;           // max speed at constant v (rad/s),standard disk at 7200 rpm
    [HideInInspector]
    public float Inertia;


    // Speed and acceleration of the disk at the different PWM rates on the teensy
    // 20 - 90 % -> to rad/s and acc on rad/s^2
    //                              |  20  |  30  |  40  |  50   |  60   |   70  |   80   |  90  |  95  |    
    float[] yawVel = new float[] { 2.52f, 5.47f, 08.56f, 11.75f, 15.09f, 18.02f, 20.95f, 23.90f, 26.20f };
    float[] yawAcc = new float[] { 4.15f, 8.12f, 12.02f, 15.41f, 19.21f, 22.59f, 25.79f, 28.69f, 30.63f };
    float[] pitchVel = new float[] { 2.66f, 5.10f, 07.71f, 09.91f, 12.51f, 15.04f, 17.84f, 19.84f, 21.15f };
    float[] pitchAcc = new float[] { 4.15f, 8.12f, 12.02f, 15.41f, 19.21f, 22.59f, 25.79f, 28.69f, 30.63f };

    // // predefine moving speed settings, at 40% 
    // float yawCollSpeed = 8.56f;
    // float pitchCollSpeed = 7.71f;
    // // at 20% 
    // float yawMovSpeed = 2.52f;
    // float pitchMovSpeed = 2.66f;

    // classes used on the controller
    [HideInInspector]
    public HelperMethods helperMethods = null;
    [HideInInspector]
    public CollisionControl collisionControl = null;
    [HideInInspector]
    public GyroModeling gyroModeling = null;

 
    private void Start()
    {
        //Debug.Log("start fo the program");
        Inertia = diskMass * (radius * radius);

        _instance = this;

        helperMethods = gameObject.AddComponent<HelperMethods>();
        collisionControl = gameObject.AddComponent<CollisionControl>();
        gyroModeling = gameObject.AddComponent<GyroModeling>();
    }

    private void Update() {
        YawRot = YawAxle.Rot;
        //YawVel = YawAxle.Vel;
        //YawAcc = YawAxle.Acc;
        PitchRot = PitchAxle.Rot;
        // PitchVel = PitchAxle.Vel;
        // PitchAcc = PitchAxle.Acc;
    }

    private void FixedUpdate()
    {
        // update the closest obj to look at
        collisionControl.UpdtClosestObj();
        // rotate gimbal to look the closest obj to the paddle
    }

    #region Com for the UI   
    public void sendYaw(float msg)
    {
        OscMessage message = new OscMessage();
        message.address = "/yawS";
        message.values.Add(msg);
        SceneManager.instance().osc.SendOSCMessage(message);
    }
    public void sendPitch(float msg)
    {
        OscMessage message = new OscMessage();
        message.address = "/pitchS";
        message.values.Add(msg);
        SceneManager.instance().osc.SendOSCMessage(message);
    }
    public void sendDiskVel(float msg)
    {
        OscMessage message = new OscMessage();
        message.address = "/yawS";
        message.values.Add(msg);
        SceneManager.instance().osc.SendOSCMessage(message);
    }
    public void sendGimVel(float msg)
    {
        OscMessage message = new OscMessage();
        message.address = "/G_vel";
        message.values.Add(msg);
        SceneManager.instance().osc.SendOSCMessage(message);
    }
    public void sendPid(int msg)
    {
        OscMessage message = new OscMessage();
        message.address = "/sPID";
        message.values.Add(msg);
        SceneManager.instance().osc.SendOSCMessage(message);
    }
    #endregion
}