﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// When a tennis ball is instantiated a force is applied to launch it along its 
/// Z axis, upon collision it destroys itself 
/// </summary>
public class TennisBall : MonoBehaviour
{

    Vector2 velRange = new Vector2(2, 4);
    GameObject flywheel;

    void Awake()
    {
        // we want to keep track and re-orientate the flywheel 
        flywheel = GameObject.Find("Flywheel");
        if (flywheel == null) Debug.LogError("no flywheel found");
    }

    // start by launching the ball to the disk
    private void Start()
    {
        float appliedForce = Random.Range(velRange.x, velRange.y);
        GetComponent<Rigidbody>().AddRelativeForce(new Vector3(0.0f, 0.0f, appliedForce), ForceMode.Force);
        GyroController.instance().collisionControl.AddCollisionObj(gameObject);
    }

    void OnCollisionEnter(Collision coll)
    {
        GetComponent<Rigidbody>().useGravity = true;
        GetComponent<Collider>().enabled = false;
        Destroy(gameObject, 2);
    }
}