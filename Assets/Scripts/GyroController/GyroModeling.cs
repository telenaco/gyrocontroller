﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GyroModeling : MonoBehaviour
{

    // List<GyroData> gyroCollData = new List<GyroData>();
     List<Vector3> torqueList = new List<Vector3>();

    // Momentum generated when the flywheel is actuated
    Vector3 outModel;

    // a collison.impact on unity represents the instant energy                
    Vector3 collisionPeak;
    float incrementPitch;

    GyroController GyC;


    private void Start(){
        GyC = GyroController.instance();
    }

    private void Update() {

        //Vector3 direction = (Destination.position - transform.position).normalized;
    }

    void OnCollisionEnter(Collision coll)
    {
        // ???!! sort out the thinking with what happen in what coordinate frame to convert it

        //0. Compute torque in WORLD coordinates
        // since "impulse = force * time"
        Vector3 collisionForce = coll.impulse / Time.fixedDeltaTime;

        // sometimes the collision force is 0, this seems to be a known issue wiht unity 
        // ignore it if its the case, remove the obj from the list and search for next object
        if (collisionForce.magnitude == 0.0f)        
            return;
        

        // draw the collision force vector on the scene 
        // GyC.helperMethods.DrawArrow((coll.contacts[0].point + collisionForce), (-collisionForce), 0.01f, Color.blue, 10.0f, false);

        collisionPeak = calcTorque(collisionForce);

        // show direction
        Debug.DrawRay(transform.position, collisionPeak.normalized , Color.green, 5.0f);

        //MOVE CONTROLLER WITH THE MAGNITUDE OF THE IMPACT
    }

    /// <summary>
    /// provided a collision force vector3 output the torque on wold coordinates (if controller moves the torque is recalculate)
    /// </summary>
    /// <param name="_inputVector"></param>
    /// <returns></returns>
    public Vector3 calcTorque(Vector3 _inputVector)
    {
        // distance from disk origin to handle origin 
        Vector3 rVector = transform.Find("YawRing/PitchRing/Flywheel").transform.position - transform.position;
        // We know the flywheel is offset by  0.13 Y on original position on local unity coordinates
        // torque_W = r x F
        Vector3 outputTorque_W = Vector3.Cross(rVector, _inputVector);
        // GyC.helperMethods.DrawArrow(transform.position, outputTorque_W, 0.01f, Color.green, 3.0f, false);

        //1. Represent torque in devices' coords (handle in Unity "axis")
        //Vector4 outputTorque_Paddle = transform.worldToLocalMatrix * (new Vector4(outputTorque_W.x, outputTorque_W.y, outputTorque_W.z, 0.0f));
        // returns an output torque on unity world coordinates
        //Vector3 outM =  new Vector3(outputTorque_Paddle.x, outputTorque_Paddle.y, outputTorque_Paddle.z);
        //return outM;
        return outputTorque_W;
    }

    
    // 1
    // Calculate the model output momentum {\overrightarrow{\bf {M}}{^{gyro}_{A}} 
    // and rotate it flywheel->hand->world coordinates, this is the torque the user feels as a result of the wheel actuation 
    public Vector3 modelTorque(Vector3 kinRot = default(Vector3))
    {
        // CONTROLLER CF
        float PitchRot = GyC.PitchAxle.Rot + kinRot.y;
        float YawRot = GyC.PitchAxle.Rot + kinRot.x;
        float PitchVel = 1.0f;
        float YawVel = 1.0f;
        float PitchAcc = 0.1f;
        float YawAcc = 0.1f;

        // this is calculated on the controller frame of reference at the flywheel origin
        outModel.x = (0.25f * GyC.Inertia) *
            ((2.0f * PitchVel * GyC.diskAngVel) -
            (YawAcc * Mathf.Sin(GyC.PitchAxle.Rot)));

        outModel.y = (0.25f * GyC.Inertia) *
            (PitchAcc +
            ((YawVel * Mathf.Sin(GyC.PitchAxle.Rot) *
            ((2.0f * GyC.diskAngVel) + (YawVel * Mathf.Cos(GyC.PitchAxle.Rot))))));

        outModel.z = (0.5f * GyC.Inertia) *
            ((YawAcc * Mathf.Cos(GyC.PitchAxle.Rot)) -
            (PitchVel * YawVel * Mathf.Sin(GyC.PitchAxle.Rot)));    


        // DEBUGING 
        // PLOT THE OUTPUT MOMENTUN AS A CHILD OF THE FLYWHEEL
        // Quaternion rotFly = GyC.PitchAxle.transform.rotation;
        //GyC.helperMethods.DrawArrow(GyC.PitchAxle.transform.position, rotFly * outModel.normalized * 10.0f, 0.01f, Color.blue, 0.0f, false);

        // validated with the ATI readings, should change on the X, Z plane of the controller coordinates https://www.notion.so/telenaco/Testing-ATI-Gyro-Data-Analysis-6312a69d37e846f996f4109d289d8173

        // rotate from disk to handle local coordinates
        outModel = GyC.helperMethods.rotateA_to_D(outModel);
        outModel = GyC.helperMethods.unityToController(outModel);
        // DEBUGING

        //GyC.helperMethods.DrawArrow(GyC.transform.position, outModel * 10.0f, 0.01f, Color.green, 0.0f, false);

        // OUTPUT TORQUE TO WORLD COORDINATES 
        Quaternion rot = GyC.transform.rotation;
        outModel = rot * outModel;
        // DEBUGING
        GyC.helperMethods.DrawArrow(transform.position, outModel.normalized, 0.01f, Color.red, 5.0f, false);

        return outModel;
    }

    // // public void singleImpact(){
    // //     incrementPitch += 0.2f;
    // //     if (incrementPitch <= 45.0f) {
    // //         GyC.sendPitch(45.0f);
    // //     } else {
    // //         GyC.isColliding = false;
    // //         incrementPitch = 0.0f;

    // //         GyC.sendGimVel(22.0f);
    // //         GyC.sendPid(1);
    // //         GyC.sendPitch(0.0f);
    // //     }
    // // }

    // // We receive a collision, populate a list with the torques to deliver per 
    // // frame. Use to actuate the flywheel on collision
    // public void UpdtCollList(Vector3 _receivedTorque)
    // {
    //     // ***ignore
    //     // Constrain the magnitude of the received torque to max 10 (arbitrary number)
    //     // float collisionMagnitude = _receivedTorque.magnitude;
    //     // Calculate the total time to deliver the torque
    //     // float TorqueDeliveryTime = mapTimeStep(collisionMagnitude, 0.0f, 10.0f, 0.8f, 0.5f);
    //     //***

    //     // ▲t =0.013333
    //     float numOfSteps = 80.0f;
    //     // increment linearly the torque each step
    //     Vector3 incrementStep = _receivedTorque / numOfSteps;
    //     // clear the list of previous values
    //     torqueList.Clear();
    //     // set first value and add the rest
    //     torqueList.Add(new Vector3(0.0f, 0.0f, 0.0f));
    //     // fill list with values
    //     for (int i = 1; i < numOfSteps; i++)
    //     {
    //         torqueList.Add(torqueList[i - 1] + incrementStep);
    //     }
    //     // calculate the required acceleration of the axles
    //     //torqueToAcc();
    // }


    // //    void torqueToAcc()
    // void calcAcc()
    // {

    // }

    // void diskTransformUpdate() {
    //     Vector3 newRotation = new Vector3(GyC.gyroData.YawRot, GyC.gyroData.PitchPos, 0);
    //     GyC.diskObj.transform.eulerAngles = newRotation;
    // }

    //#region Model output torque based on disk movement

    // if we are simulating the outup momentum we run in order the following methods


}
