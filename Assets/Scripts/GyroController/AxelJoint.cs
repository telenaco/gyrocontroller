// /*------------------------------------------- 
// --------------------------------------------- 
// Creation Date:  
// Author: Luis 
// Description: GyroController 
// --------------------------------------------- 
// -------------------------------------------*/
// CONTROLLER DATA IS STORED IN CONTROLLER COORDINATE FRAME 
// RIGHT HAND Z UP

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

public class AxelJoint : MonoBehaviour
{
    public float Rot, Vel, Acc;
    public Vector3 Axis;
    Quaternion _QRot, _prevQRot;
    Vector3 _Vel, _prevVel, _Acc, _prevAcc;

    [Header("Initial position")]
    // The offset at resting position
    [ReadOnly]
    public Vector3 StartOffset;

    // The initial one
    [ReadOnly]
    public Vector3 ZeroEuler;

    // weight filter
    [Header("Vel Filtering weight")]
    [Range(0.0f, 1.0f)]
    public float weightVel = 0.1f;
    // weight filter
    [Header("Acc Filtering weight")]
    [Range(0.0f, 1.0f)]
    public float weightAcc = 0.1f;

    [Header("Rotate Axis")]
    [Range(-100.0f, 100.0f)]
    public float rotateSpeed = 0.0f;
    public bool rotateAxis = false;

    private void Start()
    {
        ZeroEuler = transform.localEulerAngles;
        StartOffset = transform.localPosition;
        _QRot = transform.localRotation;
        _prevQRot = _QRot;
        _prevVel = Vector3.zero;
        _prevAcc = Vector3.zero;
    }

    void Update()
    {
        // update current rotation
        _QRot = GyroController.instance().helperMethods.unityToController(transform.localRotation);

        // difference from previous position, substract current quaternion from prev 
        Quaternion deltaRotation = _QRot * Quaternion.Inverse(_prevQRot);

        // Calculates the shortest difference between two given angles given in degrees.
        // avoid angle > 360 step
        Vector3 eulerDeltaRotation = new Vector3(
            Mathf.DeltaAngle(0, deltaRotation.eulerAngles.x),
            Mathf.DeltaAngle(0, deltaRotation.eulerAngles.y),
            Mathf.DeltaAngle(0, deltaRotation.eulerAngles.z));


        // Physics are calculated using radians
        Vector3 radiansRotation = eulerDeltaRotation * Mathf.Deg2Rad;

        // calculate velocity and acceleration on rad/s
        _Vel = radiansRotation / Time.fixedDeltaTime;
        // low pass filter
        _Vel = (weightVel * _prevVel) + ((1.0f - weightVel) * _Vel);

        _Acc = (_Vel - _prevVel) / Time.fixedDeltaTime;
        _Acc = (weightAcc * _prevAcc) + ((1.0f - weightAcc) * _Acc);

        _prevQRot = _QRot;
        _prevVel = _Vel;

        if (Axis.x == 1)
        {
            Rot = _QRot.eulerAngles.x * Mathf.Deg2Rad;
            Vel = _Vel.x;
            Acc = _Acc.x;
        }
        else if (Axis.y == 1)
        {
            Rot = _QRot.eulerAngles.y * Mathf.Deg2Rad  ;
            Vel = _Vel.y;
            Acc = _Acc.y;
        }
        else if (Axis.z == 1)
        {
            Rot = _QRot.eulerAngles.z * Mathf.Deg2Rad;
            Vel = _Vel.z;
            Acc = _Acc.z;
        }

        // DEBUGGING
        if (rotateAxis)
        {
            // change rotation to controller CF
            Vector3 rotSpeed = Axis * -rotateSpeed;
            rotSpeed = GyroController.instance().helperMethods.unityToController(rotSpeed);

            Quaternion newRot = Quaternion.Euler(rotSpeed *  Time.fixedDeltaTime);
            transform.rotation *= newRot;
        }
    }

    /// <summary>
    /// Rotates N degrees the controller over the selected axis on the controller coordinate frame
    /// </summary>
    /// <param name="angle"> degrees to rotate from unity coordinate frame </param>
    /// <returns></returns>
    public float SetDegrees(float angle)
    {
        Vector3 rotAngle = Axis * -angle;
        rotAngle = GyroController.instance().helperMethods.unityToController(rotAngle);

        // rotate to new angle
        if (Axis.x == 1) transform.localRotation = Quaternion.Euler(rotAngle);
        else
        if (Axis.y == 1) transform.localRotation = Quaternion.Euler(rotAngle);
        else
        if (Axis.z == 1) transform.localRotation = Quaternion.Euler(rotAngle);

        return angle;
    }

    /// <summary>
    /// Rotates N radians the controller over the selected axis on the controller CF
    /// </summary>
    /// <param name="radians"> degrees to rotate </param>
    /// <returns></returns>
    public float SetRadians(float radians)
    {
        // rotate from unity to controller CF
        Vector3 rotAngle = Axis * (-radians * Mathf.Rad2Deg);
        rotAngle = GyroController.instance().helperMethods.unityToController(rotAngle);

        // rotate to new angle
        if (Axis.x == 1) transform.localRotation = Quaternion.Euler(rotAngle);
        else
        if (Axis.y == 1) transform.localRotation = Quaternion.Euler(rotAngle);
        else
        if (Axis.z == 1) transform.localRotation = Quaternion.Euler(rotAngle);

        return radians;
    }

    /// <summary>
    /// returns current angle in the controller CF 
    /// </summary>
    /// <returns></returns> 
    public float GetDegrees()
    {
        float angle = 0;

        Quaternion rotation = GyroController.instance().helperMethods.unityToController(transform.rotation);

        if (Axis.x == 1) angle = rotation.eulerAngles.x;
        else
        if (Axis.y == 1) angle = rotation.eulerAngles.y;
        else
        if (Axis.z == 1) angle = rotation.eulerAngles.z;

        return angle;
    }

    // https://forum.unity.com/threads/manually-calculate-angular-velocity-of-gameobject.289462/
    // internal static Vector3 GetAngularVelocity(Quaternion foreLastFrameRotation, Quaternion lastFrameRotation)
    // {
    //     var q = lastFrameRotation * Quaternion.Inverse(foreLastFrameRotation);
    //     // no rotation?
    //     // You may want to increase this closer to 1 if you want to handle very small rotations.
    //     // Beware, if it is too close to one your answer will be Nan
    //     if (Mathf.Abs(q.w) > 0.999999995f)
    //         return new Vector3(0, 0, 0);
    //     float gain;
    //     // handle negatives, we could just flip it but this is faster
    //     if (q.w < 0.0f)
    //     {
    //         var angle = Mathf.Acos(-q.w);
    //         gain = -2.0f * angle / (Mathf.Sin(angle) * Time.deltaTime);
    //     }
    //     else
    //     {
    //         var angle = Mathf.Acos(q.w);
    //         gain = 2.0f * angle / (Mathf.Sin(angle) * Time.deltaTime);
    //     }
    //     Vector3 tmp = new Vector3(q.x * gain, q.y * gain, q.z * gain);

    //     if (float.IsNaN(tmp.x) || float.IsNaN(tmp.y) || float.IsNaN(tmp.z) )
    //         return new Vector3(0.0f, 0.0f, 0.0f);
    //     else 
    //         return tmp;
    // }
}


