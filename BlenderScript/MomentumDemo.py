from cmath import pi
import bpy
import bmesh
import math
import mathutils

## Get start frame and end frame of the animation
frame_start = bpy.context.scene.frame_start
frame_end = bpy.context.scene.frame_end

## moving axles
pitch = bpy.data.objects["PitchRing"]
yaw = bpy.data.objects["YawRing"]

## controller properties
yaw_current_rot = []
pitch_current_rot = []
yaw_current_vel = []
pitch_current_vel = []
yaw_current_acc = []
pitch_current_acc = []

## output momentum
diskMomentum = []
diskMomentum.append(mathutils.Vector((0.0, 0.0, 0.0)))

## flywheel properties
deltaTime = 0.01
diskAngVel = 753

diskMass = 0.096
radius = 0.05
Inertia = diskMass * radius**2

## new mesh to plot the momentum
C = bpy.context
# Create new connector mesh and mesh object and link to scene
m = bpy.data.meshes.new('connector')

bm = bmesh.new()
v1 = bm.verts.new(pitch.location)
v2 = bm.verts.new(diskMomentum[0])
e = bm.edges.new([v1, v2])

bm.to_mesh(m)

o = bpy.data.objects.new('connector', m)
## parent it to handle or flywheel depending on the rotation applied
o.parent = bpy.data.objects["handleCoordinates"]
#o.parent = bpy.data.objects["Flywheel"]

C.scene.collection.objects.link(o)

## Iterate on the scene frames
for f in range(frame_start - 1, frame_end + 1):
    bpy.context.scene.frame_set(f)

    # Get object's current location
    pitch_current_rot.append(pitch.rotation_euler.y)
    yaw_current_rot.append(yaw.rotation_euler.z)

    # Calculate angular velocity and angular acceleration based on position
    if f >= 1:
        pitch_current_vel.append((pitch_current_rot[f] - pitch_current_rot[f - 1]) / deltaTime)
        yaw_current_vel.append((yaw_current_rot[f] - yaw_current_rot[f - 1]) / deltaTime)
    if f >= 2:
        pitch_current_acc.append((pitch_current_vel[f - 1] - pitch_current_vel[f - 2]) / deltaTime)
        yaw_current_acc.append((yaw_current_vel[f - 1] - yaw_current_vel[f - 2]) / deltaTime)

    ## once we have data for the angularVel and angularAcc calculate the output momentum
    if f >= 2:

        ################ SHORT EQUATION ############################################################
        x = ((0.25 * Inertia) *
             ((2.0 * pitch_current_vel[f - 1] * diskAngVel) -
             (pitch_current_acc[f - 2] * math.sin(pitch_current_rot[f]))))

        y = ((0.25 * Inertia) *
             (pitch_current_acc[f - 2] +
             ((yaw_current_vel[f - 1] * math.sin(pitch_current_rot[f]) *
               ((2.0 * diskAngVel) + (yaw_current_vel[f - 1] * math.cos(pitch_current_rot[f])))))))

        z = ((0.5 * Inertia) *
             ((yaw_current_acc[f - 2] * math.cos(pitch_current_rot[f])) -
             (pitch_current_vel[f - 1] * yaw_current_vel[f - 1] * math.sin(pitch_current_rot[f]))))

        ############### LONG EQUATION ###############################################################

        # mat1 = mathutils.Vector((Inertia * 0.25, Inertia * 0.25, Inertia * 0.5))
        

        # vec2 = mathutils.Vector()
        # vec2.x = -yaw_current_acc[f - 2] * math.sin(pitch_current_rot[f]) - pitch_current_vel[f - 1] * yaw_current_vel[f - 1] * math.cos(pitch_current_rot[f])
        # vec2.y = pitch_current_acc[f - 2]
        # vec2.z = yaw_current_acc[f - 2] * math.cos(pitch_current_rot[f]) - pitch_current_vel[f - 1] * yaw_current_vel[f - 1] * math.sin(pitch_current_rot[f])

        # vec3 = mathutils.Vector()
        # vec3.x = -yaw_current_vel[f - 1] * math.sin(pitch_current_rot[f])
        # vec3.y = pitch_current_rot[f]
        # vec3.z = yaw_current_vel[f - 1] * math.cos(pitch_current_rot[f])

        # vec4 = mathutils.Vector()
        # vec4.x = -yaw_current_vel[f - 1] * math.sin(pitch_current_rot[f])
        # vec4.y = pitch_current_rot[f]
        # vec4.z = diskAngVel + yaw_current_vel[f - 1] * math.cos(pitch_current_rot[f])

        # tmp1 = vec2.dot(mat1)
        # tmp2 = vec4.dot(mat1)
        # tmp3 = vec3.cross(tmp2)
        # out = tmp3 + tmp1

        ## matrix rotating first Y, then Z
        # mat_rot1 = mathutils.Euler((0, pitch_current_rot[f], yaw_current_rot[f]), 'YZX').to_matrix()
        # print(mat_rot1)

        # Euler rotation matrix above outputs exactly the same results
        # note to self
        mat_rot = mathutils.Matrix()
        mat_rot[0][0] = math.cos(pitch_current_rot[f]) * math.cos(yaw_current_rot[f])
        mat_rot[0][1] = -math.sin(yaw_current_rot[f])
        mat_rot[0][2] = math.sin(pitch_current_rot[f]) * math.cos(yaw_current_rot[f])
        mat_rot[1][0] = math.cos(pitch_current_rot[f]) * math.sin(yaw_current_rot[f])
        mat_rot[1][1] = math.cos(yaw_current_rot[f])
        mat_rot[1][2] = math.sin(pitch_current_rot[f]) * math.sin(yaw_current_rot[f])
        mat_rot[2][0] = math.sin(pitch_current_rot[f])
        mat_rot[2][2] = math.cos(pitch_current_rot[f])
        #print(mat_rot)

        vec = mathutils.Vector((x, y, z))
        #mat_trans = mathutils.Matrix.Translation(vec)

        mat_out = vec @ mat_rot

        # mat3 = mat.to_3x3()

        # # out = mat_rot @ tmp
        # # print("handle", out)

        # return rot_a_to_d * _input;

        diskMomentum.append(vec *100)


for f in range(frame_start, frame_end - 10):

    # Select one vertex from the vertex list and decide on which axis of xyz to move
    m.vertices[0].co.x = diskMomentum[f].x
    m.vertices[0].co.y = diskMomentum[f].y
    m.vertices[0].co.z = diskMomentum[f].z
    m.update(calc_edges=True)
    # Insert the movement of the selected vertex into a keyframe with the selected axis and the number of frames
    m.vertices[0].keyframe_insert('co', index=-1, frame=f)

print("\n", "\n")

print("END OF SCRIPT")
