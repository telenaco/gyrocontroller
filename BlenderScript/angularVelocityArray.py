import bpy
from math import pi
from mathutils import Vector

## Get start frame and end frame of the animation
frame_start = bpy.context.scene.frame_start
frame_end = bpy.context.scene.frame_end

radToDeg = 180 / pi

pitch = bpy.data.objects["PitchRing"]
yaw = bpy.data.objects["YawRing"]
pitchEuler = pitch.rotation_euler
yawEuler = yaw.rotation_euler
vPitchEuler = Vector(pitchEuler)
vPitchEuler *= radToDeg
print(vPitchEuler)

previous_rotation = []
current_rotation = []


# for f in range(frame_start-1, frame_end+1):
#     bpy.context.scene.frame_set(f)

#current_rotation[f] = f/2
#print("Frame %i - rotation" % f)
#print (current_rotation[f])

#        bpy.context.scene.frame_set(f)
#        print("Frame %i - velocity" % f)
#
#        # Switch arrays with location info
#        previous_locations = current_locations.copy();
#        current_locations = []
#
#        # Loop through the selected objects
#        index = 0
#        for o in selected_objects:
